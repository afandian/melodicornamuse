# Melodicornamuse

Selection of audio projects around ESP32 and MAX98357A I2S audio chip.

## Log

## 2021-01-23T12-58 - Audio scratchy

Sometimes it sounds great, sometimes it sounds scratchy. Not sure if this is due to bad connections in the electronics, or something to do with signal generation. But the intermittent nature makes it hard to track down.

[audio](progress/2021-01-23T12-58-intermittent-scratchy.mp3)

## 2021-01-23T14-07 - Fixed scratch audio, due to leakage between channels?

By fixing the float cast from `(short)` to `(int16_t)` seems to make it work.

I assumed that `short` was the same as `int16_t` because the [SDK says so)(https://github.com/espressif/arduino-esp32/blob/master/tools/sdk/include/newlib/sys/types.h#L167):

```
typedef short int16_t;
```

However, a debug masking to 16 bits, from `buf[i] = ((short)sin_float)` to `buf[i] = ((short)sin_float) & 0xFFFF` hints that the data was somehow spilling over 16 bits into the other channel. 

[audio](progress/2021-01-23T14-07-fix-16bit.mp3)


## 2021-01-27

Allocated 156678 bytes
Setting table...
Guru Meditation Error: Core  1 panic'ed (StoreProhibited). Exception was unhandled.
Core 1 register dump:
PC      : 0x400d1144  PS      : 0x00060330  A0      : 0x800d11be  A1      : 0x3ffb1f30  
A2      : 0x3ffc06fc  A3      : 0x3ffc0754  A4      : 0x3ffc02fc  A5      : 0x00000001  
A6      : 0x0000096f  A7      : 0x00000000  A8      : 0x00000000  A9      : 0x00000000  
A10     : 0x00000000  A11     : 0x00000000  A12     : 0x38000000  A13     : 0x00000380  
A14     : 0x00000000  A15     : 0x00001133  SAR     : 0x0000001d  EXCCAUSE: 0x0000001d  
EXCVADDR: 0x00000000  LBEG    : 0x40002390  LEND    : 0x4000239f  LCOUNT  : 0x00000000  

Backtrace: 0x400d1144:0x3ffb1f30 0x400d11bb:0x3ffb1f70 0x400d1e07:0x3ffb1fb0 0x4008837d:0x3ffb1fd0

Rebooting...
ets Jun  8 2016 00:22:57


this demonsrated failure:


    Serial.printf("malloc... \n");
    this->sampleTable = (int16_t*)malloc(offset * sizeof(int16_t));
    Serial.printf("result: %d \n", this->sampleTable);
    this->sampleTable[0] = 1;
    Serial.printf("Done! \n");


malloc... 
result: 0 
Guru Meditation Error: Core  1 panic'ed (StoreProhibited). Exception was unhandled.
Core 1 register dump:
PC      : 0x400d10cd  PS      : 0x00060330  A0      : 0x800d1216  A1      : 0x3ffb1f30  
A2      : 0x3ffc06fc  A3      : 0x3ffc0754  A4      : 0x3ffc02fc  A5      : 0x00013203  
A6      : 0x00026406  A7      : 0x00000000  A8      : 0x00000001  A9      : 0x3ffb1e90  
A10     : 0x0000000b  A11     : 0x3f40115a  A12     : 0xab769c88  A13     : 0x3ffc0a88  
A14     : 0x3ffb1ecc  A15     : 0x3ffb1ecc  SAR     : 0x00000004  EXCCAUSE: 0x0000001d  
EXCVADDR: 0x00000000  LBEG    : 0x400014fd  LEND    : 0x4000150d  LCOUNT  : 0xffffffff  

Backtrace: 0x400d10cd:0x3ffb1f30 0x400d1213:0x3ffb1f70 0x400d1e5f:0x3ffb1fb0 0x4008837d:0x3ffb1fd0


malloc can fail!

allocating individually was ok

## SD and GAIN on the MAX98357

Crackling often accompanied moving wires but sometimes spontaneous.

The breakout board says there are pulldowns. but finally pulling them up did the job. fingers crossed no more crackle.

## 2021-01-01 - Start of envelopes

Rudimentary attack and release envelope, universal. Applied per-buffer, so it's a bit steppy. Could be improved by doing it per-sample.

[audio](progress/2021-02-01T21-03-long-envelopes-random-notes-60-to-70.mp3)

![progress/2021-02-01T21-03-long-envelopes-random-notes-60-to-70](progress/2021-02-01T21-03-long-envelopes-random-notes-60-to-70.png)

Demo with random notes coming on and off.



## Mistakes made

### Blocking vs interrupts

 - Spent too long assuming that ESP32's i2s_write is non-blocking and interrupt-driven, so tried to find the right interrupts to fill the buffer when it was empty.
 
### Data types

 - Short vs unt16_t
