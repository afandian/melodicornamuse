#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/i2s.h"

#include "driver/gpio.h"
#include "esp_system.h"
#include <math.h>

#define SAMPLE_RATE    ((i2s_bits_per_sample_t)36000)

// I2S peripheral (there are two).
#define I2S_NUM        (I2S_NUM_0)

#define PI             (3.14159265)

// Binary clock
#define I2S_BCK_IO     (GPIO_NUM_25)

// Word select / left-right clock
#define I2S_WS_IO      (GPIO_NUM_32)

// Digital out
#define I2S_DO_IO      (GPIO_NUM_33)

// Digital in, unused.
#define I2S_DI_IO      (-1)

// 512 is unstable
#define BUF_SIZE (256)

// Allocate single buffer, reused.
#define BITS_PER_SAMPLE  (I2S_BITS_PER_SAMPLE_16BIT)
#define BUF_BYTES (BUF_SIZE * (BITS_PER_SAMPLE / 8) * 2)

// This many pitches in a MIDI pitch value.
#define MAX_PITCH (128)

// However it's only worth rendering between these two pitches
// #define MIN_USED_PITCH (40)
// #define MAX_USED_PITCH (88)

#define MIN_USED_PITCH (60)
#define MAX_USED_PITCH (70)

// Very slow attack at the moment. 
#define ATTACK_SECONDS (1.0)
#define ATTACK_AMPLITUDE_PER_SAMPLE (ATTACK_SECONDS / ((float)SAMPLE_RATE * (float)BUF_SIZE))

float midiToFrequency(int pitch) {
  return 440.0 * pow(2, ((float)pitch - 69.0) / 12.0);
}

// A voice that comprises a single sine wave, but can play polyphonic notes.
class Voice {

  // Midi pitch -> amplitude. Used to indicate if each pitch is on or off, 
  // and how loud. 
  // Although we only need (MAX_USED_PITCH - MIN_USED_PITCH), we don't waste 
  // much by making it fully MAX_PITCH big and gain some flexibility.
  float pitchAmplitudes[MAX_PITCH];

  // The actual will lag the target due to attack and decay.
  float pitchAmplitudesActual[MAX_PITCH];

  // MIDI pitch -> number of samples in one wave at that frequency, measured in samples.
  // As we pre-calculate store one complete cycle of each MIDI pitch, and as each 
  // frequency has a different wavelength, we need to store a different buffer length.
  int samplesSize[MAX_PITCH];

  // Array of MIDI pitch to pre-calculated buffer data.
  // Stored as floats for ease of mixing and attenuation.
  // The length of sample n is samplesSize[n].
  // If we try to allocate this all at once then malloc will fail because it can't 
  // allocate a single contiguous chunk that size (e.g. 156678 bytes).
  // But splittig it up works fine.
  float *sampleTable[MAX_PITCH];

  // The time offset that the most recent tick() was called.
  // Used to calculate time delta for ADSR.
  uint32_t lastTickOffset;

public:
  void init() {
    Serial.println("Start intialize...");
    this->clear();
    this->initSamples();
    this->lastTickOffset = 0;
    Serial.println("Done initializing!");
  }

  // Pre-calculate samples for all pitches we might use.
  void initSamples() {

    // Running total of the amount of space we used. Useful for debugging OOM errors.
    int totalBytes = 0;
    int totalSamples = 0;

    for (int i = MIN_USED_PITCH; i < MAX_USED_PITCH; i++) {
      
      // Size of samples needed to store one wave of this frequency.
      int sizeSamples = (SAMPLE_RATE / midiToFrequency(i));
      int bytes = sizeSamples * sizeof(float);

      this->samplesSize[i] = sizeSamples;
      this->sampleTable[i] = (float*) malloc(bytes);

      if (this->sampleTable[i] == NULL) {
        Serial.printf("PANIC can't malloc pitch: %d, bytes: %d \n", i, bytes);
        abort();
      }

      Serial.printf("Pitch: %d , size: %d, bytes %d \n", i, sizeSamples, bytes);

      totalBytes += bytes;
      totalSamples += sizeSamples;
    }

    Serial.printf("Allocated samples: %d, bytes: %d \n", totalSamples,  totalBytes);

    int start = millis();
    Serial.printf("Creating sample table...\n");
    for (int pitch = MIN_USED_PITCH; pitch < MAX_USED_PITCH; pitch++) {

      float freq = midiToFrequency(pitch);
      int size = this->samplesSize[pitch];

      Serial.printf("Setting pitch: %d, freq: %f, size: %d \n", pitch, freq, size);

      // `size` should take us up to one cycle of the sample.
      for (int i = 0; i < size; i++) {
          float sin_float = sin((float)i * 2.0 * PI / ((float)SAMPLE_RATE / freq));
          sin_float *= (pow(2, BITS_PER_SAMPLE) / 2 - 1);
          this->sampleTable[pitch][i] = sin_float;
        }
    }
    
    int end = millis();
    Serial.printf("Creating sample table took %d ms...\n", end-start);
  }

  void clear() {
    for (int i = 0; i < MAX_PITCH; i++) {
      this->pitchAmplitudes[i] = 0.0;
    }
  }

  void on(uint8_t _pitch, float amplitude) {
    if (_pitch >= MIN_USED_PITCH && _pitch <= MAX_USED_PITCH) {
      this->pitchAmplitudes[_pitch] = amplitude;
    }
    
  }

  void off(uint8_t _pitch) {
    if (_pitch >= MIN_USED_PITCH && _pitch <= MAX_USED_PITCH) {
      this->pitchAmplitudes[_pitch] = 0.0;
    }
  }

  // Calculate the delta (in samples) since the last tick was made.
  // Modifies state so call once per tick().
  uint32_t calculateTickDelta(uint32_t offset) {
    uint32_t result = 0;

    // Offset always increases, and lastTickOffset is always the last value.
    // However offset may saturate uint32 and wrap around back to zero.
    // If this happens, detect it and just guess that there was a buffer-length.
    if (offset < this->lastTickOffset) {
      result = BUF_SIZE;
    } else {
      result = offset - this->lastTickOffset;
    }

    this->lastTickOffset = offset;
    return result;
  }

  void updateActualAmplitues(uint32_t elapsedSamples) {
    float adjustment = ATTACK_AMPLITUDE_PER_SAMPLE * elapsedSamples;

    for (int pitch = MIN_USED_PITCH; pitch < MAX_USED_PITCH; pitch++) {
      // Negative means too low. Positive means too high.
      float diff = this->pitchAmplitudesActual[pitch] - this->pitchAmplitudes[pitch];
      
      // If it's under the required value, add some.
      if (diff < -0.001) {
        // Add an amount in proportion to how much time elapsed since the last envelope adjustment.
        this->pitchAmplitudesActual[pitch] += adjustment;

        // But don't overshoot.
        this->pitchAmplitudesActual[pitch] = fmin(this->pitchAmplitudesActual[pitch], this->pitchAmplitudes[pitch]);
      
      // If it's over, remove some.
      } else if (diff > 0.001) {
        // Add an amount in proportion to how much time elapsed since the last envelope adjustment.
        this->pitchAmplitudesActual[pitch] -= adjustment;

        // But don't undershoot.
        this->pitchAmplitudesActual[pitch] = fmax(this->pitchAmplitudesActual[pitch], this->pitchAmplitudes[pitch]);
      }
    }

  }

  // Fill the buffer with one BUF_SIZE worth of data, mixing all the 
  // pitches at their specified amplitudes.
  // Add, don't replace, so that this can be mixed with others.
  // Accept offset to produce correct signal phase. 
  void tick(float *buf, uint32_t offset) {

    // This many samples since the last time.
    uint32_t tickDelta = this->calculateTickDelta(offset);

    // Converge amplitudes on target.
    this->updateActualAmplitues(tickDelta);

    for (int pitch = MIN_USED_PITCH; pitch < MAX_USED_PITCH; pitch++) {
      int size = this->samplesSize[pitch];
      float amp = this->pitchAmplitudesActual[pitch];
      if (amp > 0.001) {
        for (int i = 0; i < BUF_SIZE; i++) {
          buf[i] += this->sampleTable[pitch][(i + offset) % size] * amp;
        }
      }
    }
  }
};

// A container for voices.
// Contains a reusable buffer, I2S driver. 
// Also stores a global sample counter to maintain phase. 
class Synth {
  i2s_config_t config;

  Voice voice;

  // Binary buffer to send to I2S.
  // This is allocated once and then repeatedly used to send chunks into the DMA buffer.
  int *soundBuf;

  // Intermediary monophonic float buffer that Voices write data to.
  float *preBuf;

  // Maintain number of samples written since the start of playback to maintain
  // oscillators' phase between samples. 
  // Unsigned 32 bits gives us ~27 hours at 44.1 kHz before it overflows.
  // Even when that happens it's just a 1-sample glitch.
  uint32_t samplesCount;

public:

  // Initialize voices, I2S config, and allocate necessary buffers.
  void init() {
    this->voice.init();

    this->config = {};
    this->config.mode = (i2s_mode_t) (I2S_MODE_MASTER | I2S_MODE_TX);
    this->config.sample_rate = SAMPLE_RATE;
    this->config.bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT;
    this->config.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT;
    this->config.communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB);
    this->config.dma_buf_count = 6;
    this->config.dma_buf_len = BUF_BYTES;
    this->config.use_apll = false;
    this->config.tx_desc_auto_clear = true;
    this->config.intr_alloc_flags = ESP_INTR_FLAG_LEVEL1;

    this->soundBuf = (int*) malloc(BUF_BYTES);
    this->preBuf = (float*) malloc(BUF_SIZE * sizeof(float));

    i2s_pin_config_t pin_config = {
      .bck_io_num = I2S_BCK_IO,
      .ws_io_num = I2S_WS_IO,
      .data_out_num = I2S_DO_IO,
        .data_in_num = I2S_DI_IO                                               //Not used
    };
    
    i2s_driver_install((i2s_port_t)I2S_NUM, &this->config, 0, NULL);
    i2s_set_pin((i2s_port_t)I2S_NUM, &pin_config);

    this->samplesCount = 0;
  }

  // Just a demo for now.
  // Play four simultaneous notes. Two of them random pitch and timing.
  // Two of them constant timing but random notes.
  int counter0 = 1;
  int counter1 = 1;
  int counter2 = 1;
  int counter3 = 1;
  int counter4 = 1;

  int p0 = 0;
  int p1 = 0;
  int p2 = 0;
  int p3 = 0;
  int p4 = 0;

  void tick() {

    // Demo stuff
    counter0 --;
    counter1 --;
    counter2 --;
    counter3 --;
    counter4 --;

    if (counter0 == 0) {
      counter0 = random(500, 100);
      voice.off(p0);
      p0 = random(MIN_USED_PITCH, MAX_USED_PITCH);
      voice.on(p0, 0.6);
    }

    if (counter1 == 1) {
      counter1 = random(1000, 1500);
      voice.off(p1);
      p1 = random(MIN_USED_PITCH, MAX_USED_PITCH);
      voice.on(p1, 0.6);
    }

    if (counter2 == 0) {
      counter2 = random(1000, 3000);
      voice.off(p2);
      p2 = random(MIN_USED_PITCH, MAX_USED_PITCH);
      voice.on(p2, 0.6);
    }

    if (counter3 == 0) {
      counter3 = random(1000, 2000);
      voice.off(p3);
      p3 = random(MIN_USED_PITCH, MAX_USED_PITCH);
      voice.on(p3, 0.6);
    }

    if (counter4 == 0) {
      counter4 = random(500, 1000);
      voice.off(p4);
      p4 = random(MIN_USED_PITCH, MAX_USED_PITCH);
      voice.on(p4, 0.6);
    }

    
    // Clear buffer each time. Voice(s) will add to it.
    memset(this->preBuf, 0, BUF_BYTES);

    // Get voice (in future maybe more voices) to populate float buffer.
    voice.tick(this->preBuf, this->samplesCount);

    // Convert float buffer into binary I2S buffer.
    for (int i = 0; i < BUF_SIZE; i++) {
      // Explcitly use 16 bit type, not short!
      // Squash 16-bit left and right channel into 32 bits.
      this->soundBuf[i] = (int16_t)this->preBuf[i];
    }


    // Fill ESP32's DMA ring buffer.
    // This blocks until this buffer has been copied into the DMA buffer.
    // portMAX_DELAY means block indefinitely until data taken.
    size_t i2s_bytes_write = 0;
    i2s_write((i2s_port_t)I2S_NUM, this->soundBuf, BUF_BYTES, &i2s_bytes_write, portMAX_DELAY);

    // For phase.
    this->samplesCount += BUF_SIZE;
  }
};


Synth synth;
void setup(void)
{
  setCpuFrequencyMhz(240);
  Serial.begin(115200);
  Serial.println("Initializing...");
  Serial.printf("Heap: %d \n", ESP.getFreeHeap());

  // Wait a little time before making loud noises so we can reprogram.
  delay(2000);
  Serial.println("Start synth");
  synth.init();
}

void loop() {
  synth.tick();
}
